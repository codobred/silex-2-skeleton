<?php
/**
 *---------------------------------------------------------------
 * SiteCatalog v 0.0.1 
 *---------------------------------------------------------------
 *
 * Bootstrapting file
 * 
 * Made by codobred (codobred@gmail.com)
 * @author codobred ( vk.com/id110775131 )
 */
 
date_default_timezone_set('Europe/Kiev');

$app = new Silex\Application;

// enable/disable errors reporting
// must be false in production
$forceDebug = true;
if ( '127.0.0.1' == getenv('REMOTE_ADDR') || $forceDebug ) {
  $app['debug'] = true;
  $app->register(new WhoopsSilex\WhoopsServiceProvider());
}

// error handler
$app->error(function() use ($app) {
    return 'wo-wo, error here';
});

// Controllers
$app->mount('/', new Controller\Main);

// Url generator
// $app->register(new Silex\Api\Provider\UrlGeneratorServiceProvider);

$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
   'http_cache.cache_dir' => __DIR__.'/cache/',
   'http_cache.esi'       => null,
));

return $app;