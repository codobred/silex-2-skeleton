<?php
/**
 *---------------------------------------------------------------
 * Retranslator v 0.0.1 ( vk.com api )
 *---------------------------------------------------------------
 *
 * Original source code
 *
 *
 * Made by codobred (codobred@gmail.com)
 * @author codobred ( vk.com/id110775131 )
 */
 
// enable/disable php errors reporting

require_once __DIR__.'/vendor/autoload.php'; 

define(ROOT__DIR, __DIR__);

$app = include __DIR__ . '/boot.php';

$app->run();