<?php
namespace Controller;

use \Silex\Application;

class Main implements \Silex\Api\ControllerProviderInterface {
    public function connect( Application $app) {
        $index = $app['controllers_factory'];
      
        $index->get('/', function( Application $app ) {
            return 'hello, kitty!';
        })->bind('main.index');

        return $index;
    }
}
